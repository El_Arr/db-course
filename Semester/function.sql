CREATE OR REPLACE FUNCTION book_shop_filler()
  RETURNS VOID AS $$
DECLARE
  t_name  VARCHAR(30);
  shop_lv INT;
  book_lv INT;
BEGIN
  shop_lv = (SELECT last_value
             FROM shop_id_seq);
  book_lv = (SELECT last_value
             FROM book_id_seq);

  FOR i IN 1..shop_lv LOOP
    t_name = (SELECT shop.name
              FROM shop
              WHERE shop.id = i);

    FOR j IN 1..book_lv LOOP
      INSERT INTO book_shop (shop_id, book_id, link)
      VALUES (i, j, ('www.' || t_name || '.com/book?id=' || j));
    END LOOP;
  
  END LOOP;
  RETURN;
END;
$$ LANGUAGE plpgsql;

SELECT *
FROM book_shop_filler();