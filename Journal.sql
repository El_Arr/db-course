﻿1. Templates

	CREATE TABLE employee (
		id integer NOT NULL PRIMARY KEY,
		name varchar (32) NOT NULL,
		salary integer,
		mngr_id integer,
		dep_id integer
	);
	
	INSERT INTO employee (id, name, salary) 
	values	(1, 'Андрей', 56), (2, 'Тимур', 86), (3, 'Булат', 64), (4, 'Влад', 95), (5, 'Саша', 91);
	
	SELECT salary FROM employee WHERE name = 'Андрей';
	
	UPDATE employee
		SET salary = 92
			WHERE name = 'Саша';
			///
			WHERE name IN ('...', '...', ...);
	
	DELETE FROM employee WHERE name = 'Влад';
	
	ALTER TABLE employee ADD COLUMN *name* *datatype*;

	ALTER TABLE employee
	ADD CONSTRAINT DEPARTMENT_ID_FK
	FOREIGN KEY (dep_id) 
	REFERENCES department(id);

	CREATE OR REPLACE FUNCTION process_emp_audit() RETURNING TRIGGER AS $emp_audit$
		BEGIN
			--
			-- Создаём строку в emp_audit, которая отражает выполненныю операцию
			-- Воспользуемся переменной TG_OP для определения типа операции
			--
			IF (TG_OP = 'DELETE') THEN
				INSERT INTO emp_audit SELECT 'D', now(), user, OLD.*;
				RETURN OLD;
			ELSIF (TG_OP = 'UPDATE') THEN
				INSERT INTO emp_audit SELECT 'U', now(), user, NEW.*;
				RETURN NEW;
			ELSIF (TG_OP = 'INSERT') THEN
				INSERT INTO emp_audit SELECT 'I', now(), user, NEW.*;
				RETURN NEW;
			END IF;
			-- Валидация данных
			-- IF NEW.price <= 0 THEN
			--	   RAISE EXCEPTION '% requires minimum 1', NEW.qty;
			-- END IF;
			RETURN NULL; -- Возвращаемое значение для триггера After не имеет значения
		END;
	$emp_audit$ LANGUAGE plpgsql;
	CREATE TRIGGER emp_audit
	AFTER INSERT OR UPDATE OR DELETE ON emp
	FOR EACH ROW EXECUTE PROCEDURE process_emp_audit();

2. Tasks

	21.09.2017

		1.
			SELECT max(salary) FROM employee WHERE dep_id = 16;
		
		2.	
			SELECT AVG(salary), dep_id FROM employee WHERE salary > 50 GROUP BY (dep_id);
		
		3.
			SELECT AVG(salary) GROUP BY (dep_id) HAVING AVG(salary) > 78;
	
		4.
			ALTER TABLE employee
			ADD CONSTRAINT manager_employee
			FOREIGN KEY (manager_id)
			REFERENCES employee(id);

		5.
			UPDATE employee
			SET mngr_id =(SELECT id FROM employee WHERE name = 'Андрей')
			WHERE NOT name = 'Андрей';
	
		6.
			SELECT name FROM employee 
			WHERE salary > (
				SELECT salary FROM employee 
				WHERE name = (
					SELECT name FROM employee 
					WHERE id = (
						SELECT AVG(mngr_id) FROM employee
					)
				)
			);
			// or //
			SELECT employee.name, employee.salary, employee1.salary FROM employee 
			JOIN employee AS employee1 ON employee.mngr_id=employee1.id 
			WHERE employee.salary > employee1.salary;
	
		7.
			SELECT name, salary, dep_id
				FROM employee 
				WHERE salary = (
					SELECT max(salary)
					FROM employee
					GROUP BY (dep_id)
				)
			);

	28.09.2017

		0.	
			ALTER TABLE employee
			ADD CONSTRAINT DEPARTMENT_ID_FK
			FOREIGN KEY (dep_id) 
			REFERENCES department(id);

		1.
			SELECT department.name 
			FROM department JOIN employee ON employee.dep_id=department.id
			GROUP BY department.name
			HAVING COUNT(employee.name) <= 4;

		2.
			WITH sum_salary as (SELECT SUM(salary), dep_id FROM employee GROUP BY dep_id)
			SELECT department.name FROM department 
			JOIN sum_salary ON sum_salary.dep_id=department.id
			WHERE sum = (SELECT MAX(sum) FROM sum_salary);

		3.
			WITH manager_id AS (
					SELECT DISTINCT mngr_id AS id FROM employee
				),
				 manager AS (
					SELECT employee.id AS id, dep_id FROM employee, manager_id
					WHERE employee.id = manager_id.id
				)
			SELECT employee.name FROM employee
			JOIN manager ON employee.mngr_id = manager.id
			WHERE manager.dep_id != employee.dep_id

		4. 
			JOIN ON table_1.id != table_2.id

	06.10.2017

		1. 	
			CREATE TABLE film (
				title varchar (32) NOT NULL PRIMARY KEY,
				year integer NOT NULL,
				length integer NOT NULL,
				genre varchar (32) NOT NULL,
				studio_name varchar (64) NOT NULL
			);

			CREATE TABLE movie_star (
				name varchar (64) NOT NULL PRIMARY KEY,
				movie varchar (64) NOT NULL,
				FOREIGN KEY (movie) REFERENCES movie(title)
			);

			CREATE TABLE premiere (
				cinema varchar (32) NOT NULL PRIMARY KEY,
				movie varchar (32) NOT NULL,
				date varchar (8),
				price real,
				FOREIGN KEY (movie) REFERENCES movie(title)
			);

		2. 
			ALTER TABLE movie_star ADD COLUMN salary real;

			ALTER TABLE movie ADD COLUMN cost real;

	30.10.17

    		-- for user(id, username, password, created_at)
		CREATE OR REPLACE FUNCTION user_pass() 
		RETURNING TRIGGER as $$
		BEGIN
			IF (TG_OP = 'UPDATE') THEN
				IF NEW.password = OLD.password THEN
					NEW.password := md5(NEW.password);
				END IF;
			ELSIF (TG_OP = 'INSERT') THEN
				NEW.password := md5(password);
				NEW.created_at := now();
			END IF;		
			RETURN NEW;
		END;
		$$ LANGUAGE plpgsql;
		CREATE TRIGGER user_pass 
		BEFORE INSERT OR UPDATE ON user 
		FOR EACH ROW EXECUTE PROCEDURE user_pass;

		CREATE OR REPLACE FUNCTION product_history_log() 
		RETURNING TRIGGER as $$
		BEGIN
			IF (TG_OP = 'UPDATE') THEN
				IF NEW.id != OLD.id THEN
					
				ELSIF NEW.title != OLD.title THEN
				
				ELSIF NEW.price != OLD.price THEN
					
				END IF;
			ELSIF (TG_OP = 'INSERT') THEN
				NEW.password := md5(password);
				NEW.created_at := now();
			END IF;		
		END;
		$$ LANGUAGE plpgsql;
		CREATE TRIGGER product_history_log 
		BEFORE UPDATE OR DELETE ON product 
		FOR EACH ROW EXECUTE PROCEDURE user_pass;

		CREATE OR REPLACE FUNCTION user_log()
		RETURNS TRIGGER AS $$
		DECLARE
			action_str   VARCHAR(50);
			operated_str VARCHAR(100);
			result_str   VARCHAR(150);
		BEGIN
			IF tg_op = 'INSERT'
			THEN
				operated_str := NEW.login;
				action_str := 'User added: ';
				result_str := action_str || operated_str;
				INSERT INTO _user_log VALUES (result_str);
				RETURN NEW;
			ELSEIF tg_op = 'UPDATE'
			THEN
				operated_str := NEW.login;
				action_str := 'User updated: ';
				result_str := action_str || operated_str;
				INSERT INTO _user_log VALUES (result_str);
				RETURN NEW;
			ELSEIF tg_op = 'DELETE'
			THEN
				operated_str := OLD.login;
				action_str := 'User removed: ';
				result_str := action_str || operated_str;
				INSERT INTO _user_log VALUES (result_str);
				RETURN OLD;
			END IF;
		END;
		$$ LANGUAGE plpgsql;
		CREATE TRIGGER user_history_log
		BEFORE INSERT OR UPDATE OR DELETE ON user_profile
		FOR EACH ROW EXECUTE PROCEDURE user_log();

	16.11.17

		CREATE OR REPLACE FUNCTION quicksort(
  			INOUT A  INT ARRAY,
  			lo NUMERIC,
  			hi NUMERIC
		) AS $$
		DECLARE
  			k NUMERIC;
  			T INT ARRAY;
  			p NUMERIC;
		BEGIN
  			IF (lo < hi)
  			THEN
    				p := partition(A, lo, hi);
    				k := p - 1;
    				T := quicksort(A, lo, k);
    				quicksort(A, p, hi);
  			END IF;
		END;$$
		LANGUAGE plpgsql;
		
		CREATE OR REPLACE FUNCTION partition(
			A  INT ARRAY,
			lo NUMERIC,
			hi NUMERIC
		) AS $$
		DECLARE
			pivot NUMERIC;
			i     NUMERIC;
			j     NUMERIC;
		BEGIN
			pivot := lo + hi / 2;
			i := lo;
			j := hi;
			WHILE (A [i] < pivot) LOOP
			  i := i + 1;
			END LOOP;
			WHILE (A [j] > pivot) LOOP
			  j := j - 1;
			END LOOP;
			IF (i <= j)
			THEN
			  pivot = A [i];
			  A [i] = A [j];
			  A [j] = pivot;
			  i := i + 1;
			  j := j - 1;
			END IF;
			RETURN i;
		END;$$
		LANGUAGE plpgsql;
		
		
		CREATE OR REPLACE FUNCTION sort(
		          size NUMERIC,
			INOUT A    INT ARRAY
		) AS $$
		DECLARE
			f NUMERIC;
		BEGIN
			FOR i IN 1..size LOOP
				f = 0;
				FOR j IN 1..(size - i) LOOP
					IF (A [i] > A [i + 1])
					THEN
						f = A [i];
						A [i] = A [i + 1];
						A [i + 1] = f;
						f = 1;
					END IF;
				END LOOP;
				IF (f == 0)
				THEN
				  EXIT;
				END IF;
			END LOOP;
		END;$$
		LANGUAGE plpgsql;

	14.12.2017
		|X| = NATURAL JOIN

		1.  Answer (model) = π [model] σ [speed >= 3] (pc)

		2.  Answer (maker) = π [maker]
				σ [product.model = laptop.model ^ type = 'laptop' ^ hd >= 100] (product |X| laptop)

		3.  Answer (model,price) = 
				π [model, price]
					σ [maker = 'B']
						π [model, maker, price] (
							σ [product.model = laptop.model]  (product |X| laptop)
							∪
							σ [product.model = pc.model]	  (product |X| pc)
							∪
							σ [product.model = printer.model] (product |X| printer)
						)

		4.  Answer (model) = π [model] σ [type = 'laser'] (printer)

		5.  Answer (maker) = 
				π [maker] (product)
				-
				π [maker] σ [type != 'laptop'] (product)

		6. 	T1 (model1, hd) = π [model1, hd] (PC) 
			T2 (model2, hd) = ρ [model1/model2] (T1)
			Answer (hd) =  π [hd] σ [model1 != model2] (T1 |X| T2)

		7.	T1 (model1, speed, ram) = π [model1, speed, ram] (PC) 
			T2 (model2, speed, ram) = ρ [model1/model2] (T1) 
			Answer (hd) =  π [hd] σ [model1 > model2] (T1 |X| T2)

		8.	SQL {
				WITH b AS (
    				SELECT
    				  maker,
    				  model
    				FROM product
    				  NATURAL JOIN (
    				                 (
    				                   SELECT model
    				                   FROM pc
    				                   WHERE speed >= 2.8
    				                 )
    				                 UNION
    				                 (
    				                   SELECT model
    				                   FROM laptop
    				                   WHERE speed >= 2.8
    				                 )
    				               ) AS a
				)
				SELECT DISTINCT b.maker
				FROM b
				  CROSS JOIN b AS b1
				WHERE b.model > b1.model AND b.maker = b1.maker
			}

			T1 (maker) = 
				π [model] σ [speed >= 2.8] (pc)
				∪
				π [model] σ [speed >= 2.8] (laptop)
			T2 (maker, model) = π [maker, model] (T1 |X| product)
			T3 (maker, model) = π (T2)
			Answer (maker) = π [maker] σ [T2.model > T3.model ^ T2.maker = T3.maker] (T2 X T3)

		9.	T1 (model, speed) = 
				π [model, speed] (pc)
				∪
				π [model, speed] (laptop)	
			SMax (model, speed) = ρ[speed/speed1] (
				π [model, speed1] (
					T1 x T1
					- 
					σ [speed1 < speed2] (
						ρ [speed1/speed] (T1) x ρ [speed2/speed] (T1)
					)
				)
			)
			Answer(maker) = π [maker] (SMax |X| product)

		10.	T = Product |X| PC
			T1 = π (T)
			T2 = π (T)
			T3 = π (T)
			Answer(maker) = π [T1.maker] (
				σ [T1.speed = T2.speed = T3.speed ∧ T1.maker = T2.maker = T3.maker] (
					T1 X T2 X T3 
				)
			)
