CREATE TABLE tc (
  id SERIAL NOT NULL PRIMARY KEY,
  type VARCHAR(30) NOT NULL,
  color VARCHAR(30) NOT NULL
);

INSERT INTO tc (type, color) VALUES ('dog','black'),('cat','gray'),('dog','white'),('fox','red'),('dog','black');

ALTER TABLE tc ADD COLUMN counter INT DEFAULT 0;

WITH dup_counter AS (
    SELECT
      MIN(id)  AS minId,
      type,
      color,
      COUNT(*) AS intCount
    FROM tc
    GROUP BY (type, color)
    ORDER BY (minId)
)
UPDATE tc
SET counter = (SELECT intCount
               FROM dup_counter
               WHERE dup_counter.minId = tc.id);

DELETE FROM tc
WHERE counter IS NULL;