CREATE VIEW admin_list
  AS
    SELECT *
    FROM user_profile
    WHERE is_admin;

CREATE VIEW book_published_1965
  AS
    SELECT *
    FROM book
    WHERE year_pub = 1965;

CREATE VIEW review_with_likes_35_to_45
  AS
    SELECT *
    FROM review
    WHERE 35 < like_quantity < 45;

CREATE VIEW user_most_reviewed_genre
  AS
    SELECT
      user_id,
      review.book_id AS book_id,
      genre_id,
      genre_name
    FROM review
      JOIN (SELECT
              book.id,
              genre.id,
              genre.name
            FROM book_genre
              JOIN book ON book_genre.book_id = book.id
              JOIN genre ON book_genre.genre_id = genre.id
           ) AS g(book_id, genre_id, genre_name) ON g.book_id = review.book_id
      JOIN user_profile ON user_profile.id = review.id
    WHERE user_profile.id = 4;