CREATE UNIQUE INDEX user_name_idx
  ON user_profile (username);

CREATE UNIQUE INDEX user_token_idx
  ON user_profile (token);